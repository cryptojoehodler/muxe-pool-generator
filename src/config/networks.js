export const networks = [
	{
		chain: "ethereum",
		chainId: 1,
		chainName: "Ethereum",
		currency: "ETH",
		rpcUrl: "https://api.zmok.io/mainnet/oaen6dy8ff6hju9k",
		explorer: "https://etherscan.io",
		avgBlockSeconds: 12,
		poolContractAddress: "",
		poolContractAbi: []
	},
	{
		chain: "binance-smart-chain",
		chainId: 56,
		chainName: "Binance Smart Chain",
		currency: "BNB",
		explorer: "https://bscscan.com",
		rpcUrl: "https://bsc-dataseed.binance.org",
		avgBlockSeconds: 3,
		poolContractAddress: "0x44Bd0F9E11547a4E919796e1FDace3276dbd5e45",
		poolContractAbi: [{"inputs":[],"stateMutability":"nonpayable","type":"constructor"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"swapBox","type":"address"}],"name":"NewSwapBoxContract","type":"event"},{"anonymous":false,"inputs":[{"indexed":true,"internalType":"address","name":"previousOwner","type":"address"},{"indexed":true,"internalType":"address","name":"newOwner","type":"address"}],"name":"OwnershipTransferred","type":"event"},{"inputs":[{"internalType":"contract IBEP20","name":"_stakedToken","type":"address"},{"internalType":"contract IBEP20","name":"_rewardToken","type":"address"},{"internalType":"uint256","name":"_rewardPerBlock","type":"uint256"},{"internalType":"uint256","name":"_startBlock","type":"uint256"},{"internalType":"uint256","name":"_bonusEndBlock","type":"uint256"},{"internalType":"uint256","name":"_poolLimitPerUser","type":"uint256"},{"internalType":"uint16","name":"_stakedTokenTransferFee","type":"uint16"},{"internalType":"uint256","name":"_depositFee","type":"uint256"},{"internalType":"uint256","name":"_withdrawalInterval","type":"uint256"},{"internalType":"address","name":"_admin","type":"address"}],"name":"deployPool","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[],"name":"owner","outputs":[{"internalType":"address","name":"","type":"address"}],"stateMutability":"view","type":"function"},{"inputs":[],"name":"renounceOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"},{"inputs":[{"internalType":"address","name":"newOwner","type":"address"}],"name":"transferOwnership","outputs":[],"stateMutability":"nonpayable","type":"function"}]
	},
	{
		chain: "polygon",
		chainId: 137,
		chainName: "Polygon",
		currency: "MATIC",
		explorer: "https://polygonscan.com",
		rpcUrl: "https://polygon-rpc.com",
		avgBlockSeconds: 3,
		poolContractAddress: "",
		poolContractAbi: []
	},
	{
		chain: "cronos",
		chainId: 25,
		chainName: "Cronos",
		currency: "CRO",
		explorer: "https://cronoscan.com",
		rpcUrl: "https://evm.cronos.org",
		avgBlockSeconds: 6,
		poolContractAddress: "",
		poolContractAbi: []
	}
]
