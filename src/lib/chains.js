import { networks } from './../config/networks.js';

class Providers {
	getNetworks() {
		return networks;
	};

	getNetworkDetails(chain) {
		return networks.find(obj => obj.chain == chain);
	};
}

const providers = new Providers();
export default providers;